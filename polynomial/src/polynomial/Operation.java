package polynomial;

//import java.util.*;

public class Operation 
{
	public static Polynomial add( Polynomial p1, Polynomial p2 )
	{
		Polynomial rez = p1;
		if ( p2 != null )
		{
			int i;
			for ( i = 0; i < p2.getSize(); i++ )
			{
				rez.addMonomial(p2.getMonomial(i).getCoefficient(), p2.getMonomial(i).getExponent());
			}					
		}
		return rez;
	}
	
	public static Polynomial sub( Polynomial p1, Polynomial p2 )
	{
		if ( p2 != null )
		{
			int i;
			for ( i = 0; i < p2.getSize(); i++ )
			{
				p1.addMonomial(-p2.getMonomial(i).getCoefficient(), p2.getMonomial(i).getExponent());
			}					
		}
		return p1;
	}
	
	public static Polynomial mul( Polynomial p1, Polynomial p2 )
	{
		Polynomial res = new Polynomial();
		int i, j;
		for (i = 0; i < p1.getSize(); i++)
		{
			for (j = 0; j < p2.getSize(); j++)
			{
				res.addMonomial(p1.getMonomial(i).getCoefficient() * p2.getMonomial(j).getCoefficient(), p1.getMonomial(i).getExponent() + p2.getMonomial(j).getExponent());
			}
		}
		
		return res;
	}
	
	public static Polynomial derivate( Polynomial p )
	{
		if ( p != null )
		{
			if ( p.getMonomial(0).getExponent() == 0 )
			{
				p.removeMonomial(0);
			}
			else
			{
				int i;
				for ( i = 0; i < p.getSize(); i++ )
				{
					p.getMonomial(i).setCoefficient( p.getMonomial(i).getCoefficient() * p.getMonomial(i).getExponent() );
					p.getMonomial(i).setExponent( p.getMonomial(i).getExponent() - 1 );
				}
			}
		}
		return p;
	}
	
	public static Polynomial integrate( Polynomial p )
	{
		if ( p != null )
		{
			int i;
			for ( i = 0; i < p.getSize(); i++ )
			{
				p.getMonomial(i).setCoefficient((int)( p.getMonomial(i).getCoefficient() / p.getMonomial(i).getExponent() ));
				p.getMonomial(i).setExponent((int)(p.getMonomial(i).getExponent() + 1 ));
			}
		}
		return p;
	}
	
	public static void main(String args[])
	{
		Polynomial p1 = new Polynomial();
//		Polynomial p2 = new Polynomial();
	
		p1.addMonomial(6, 6);
		p1.addMonomial(2, 2);
		p1.addMonomial(1, 1);
		p1.addMonomial(2, 2);
		p1.addMonomial(3, 3);
		p1.addMonomial(3, 1);
		p1.addMonomial(5, 5);
		p1.addMonomial(3, 3);
				
		for (int i = 0; i < 5; i++)
		{
			System.out.println(p1.getMonomial(i).getCoefficient() + " " + p1.getMonomial(i).getExponent() );
		}	
		
		p1 = integrate(p1);
		System.out.println("integrata");
		
		for (int i = 0; i < 5; i++)
		{
			System.out.println(p1.getMonomial(i).getCoefficient() + " " + p1.getMonomial(i).getExponent() );
		}	
		
	}

}