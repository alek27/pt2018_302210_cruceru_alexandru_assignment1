package polynomial;

public class Monomial 
{	
	//this class stores the pair of coefficient and exponent for a member of a polynom
	//c * x ^ e
	public Monomial( int c, int e )
	{
		coefficient = c;
		exponent = e;
	}
		
	public void setCoefficient( int c )
	{
		coefficient = c;
	}
	
	public void setExponent( int e )
	{
		exponent = e;
	}
	
	public int getCoefficient()
	{
		return coefficient;
	}
	
	public int getExponent()
	{
		return exponent;
	}
	
	private int coefficient;
	private int exponent;
}

