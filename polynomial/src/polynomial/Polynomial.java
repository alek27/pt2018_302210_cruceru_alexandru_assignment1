package polynomial;
import java.util.*;

public class Polynomial 
{
	//this class stores a list of monomialials and specific operations
	//(a0 * x ^ 0) + (a1 * x ^ 1) + (a2 * x ^ 2) + ... + (an * x ^ n)  	
	public Polynomial()
	{}
	
	public Monomial getMonomial( int index )
	{
		return polynomial.get(index);
	}
	
	public int getSize()
	{
		if ( polynomial != null)
		{
			return polynomial.size();
		}
		else
		{
			return 0;
		}
	}
	
	public void addMonomial( int c, int e ) 
	{
		int i;
		Monomial m = new Monomial(c, e);
	    for ( i = 0; i < polynomial.size(); i++) 
	    {
	        if ( e == polynomial.get(i).getExponent() ) 
	        {
	        	polynomial.get(i).setCoefficient(polynomial.get(i).getCoefficient() + c);
	        	
	            // if new element is equal to current element do nothing and
	            // return
	            return;
	        } 
	        else if ( e > polynomial.get(i).getExponent() ) 
	        	{
	            // if new element is greater than current element traverse to
	            // next.
	            continue;
	        	}
	        // code reaches here if new element is smaller than current element
	        // add element at this index and return.
	        polynomial.add(i, m);
	        return;
	    }
	    // if end of list is reached while traversing then add new element and
	    // return. (This will add element at the end of list)
	    polynomial.add(m);
	    m = null;
	}
	
	public void removeMonomial( int index )
	{
		polynomial.remove(index);
	}
	
	
	private ArrayList<Monomial> polynomial = new ArrayList<Monomial>();
}
