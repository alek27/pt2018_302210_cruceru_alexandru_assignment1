package polynomial;
//import Operation;

import java.net.URL;
import java.util.ResourceBundle;

//import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller  implements Initializable
{
	Polynomial p1 = new Polynomial();
	Polynomial p2 = new Polynomial();
	Polynomial p3 = new Polynomial();
	
    @FXML // fx:id="addPol"
    private Button addPol;

    @FXML // fx:id="subPol"
    private Button subPol;
    
    @FXML // fx:id="mulPol"
    private Button mulPol;
    
    @FXML // fx:id="derPol"
    private Button derPol;
    
    @FXML // fx:id="intPol"
    private Button intPol;
    
    @FXML // fx:id="divPol"
    private Button divPol;
    	
    @FXML // fx:id="pol1text"
    private TextField pol1text;
    
    @FXML // fx:id="pol2text"rezLabel
    private TextField pol2text;

    @FXML // fx:id="pol2text"rezLabel
    private Label rezLabel;
    
    void fillPol()
    {
    	p1 = new Polynomial();
    	p2 = new Polynomial();
    	String pol1, pol2;
    	pol1 = pol1text.getText();
		pol2 = pol2text.getText();
//    	pol1 = "3x^7+4x^1-2x^0";
    	
	    String[] parts1 = pol1.split("(-|\\+)");
	    String[] parts2 = pol2.split("(-|\\+)");

	    for (String subpart : parts1)
	    {
	        String[] part = subpart.split("x\\^");
	        int c = Integer.parseInt(part.length > 1 ? part[0] : "0");
	        int e = Integer.parseInt(part.length > 1 ? part[1] : "0");
	        p1.addMonomial(c, e);
//	        System.out.println(part.length > 1 ? part[0] : "0");
//	        System.out.println(part.length > 1 ? part[1] : "0");
	    }
	    for (String subpart : parts2)
	    {
	        String[] part = subpart.split("x\\^");
	        int c = Integer.parseInt(part.length > 1 ? part[0] : "0");
	        int e = Integer.parseInt(part.length > 1 ? part[1] : "0");
	        p2.addMonomial(c, e);
	    }
    }
    
    void printPol()
    {
    	String rez = "";
    	int i;
    	System.out.println(p3.getSize());
		for (i = p3.getSize() - 1; i >= 0; i--)
		{
			rez += p3.getMonomial(i).getCoefficient() + "x^" + p3.getMonomial(i).getExponent() + " ";
		}
		rezLabel.setText(rez);
    }
    
    @FXML
    void addPol() 
    {
		fillPol();
		p3 = Operation.add(p1, p2);
		printPol();
    }
    
    @FXML
    void subPol()
    {
		fillPol();
		p3 = Operation.sub(p1, p2);
		printPol();
    }
    
    @FXML
    void mulPol()
    {
		fillPol();
		p3 = Operation.mul(p1, p2);
		printPol();
    }
    
    @FXML
    void derPol()
    {
		fillPol();
		p3 = Operation.derivate(p1);
		printPol();
    }
    
    @FXML
    void intPol()
    {
		fillPol();
		p3 = Operation.integrate(p1);
		printPol();
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
