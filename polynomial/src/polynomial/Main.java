package polynomial;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

	@Override
	public void start(Stage stage) throws Exception 
	{	
		if (getClass().getClassLoader().getResource("Polfxml.fxml") == null)
		{
			System.out.println("null");
		}
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("Polfxml.fxml"));
	    
        Scene scene = new Scene(root, 600, 400);
    
        stage.setTitle("FXML Welcome");
        stage.setScene(scene);
        stage.show();
	}
	
	public static void main(String[] args) 
	{	
		launch(args);	
	}

}